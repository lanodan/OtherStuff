#!/bin/sh
# Outil de finition d'installation sur les nouveaux postes (HP ProDesk)
# Fait par : Haelwenn (lanodan) <contact@hacktivis.me>

echo "Déplacement de jessie à stretch"
sed -i 's/jessie/stretch/g' /etc/apt/sources.list
sed -i 's/Jessie/Stretch/g' /etc/apt/sources.list
echo "Mise à jour de la liste des paquets"
apt-get update
echo "Mise à jour des paquets"
apt-get upgrade -y
echo "Installation du nouveau noyau"
apt-get install -y linux-image-amd64
echo "Installation des outils pour l'impression réseau"
apt-get install -y libnss-mdns libavahi-compat-libdnssd1
